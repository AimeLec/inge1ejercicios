!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'gabriel 9/18/2022 20:33:16'!
test01SentenceFinderShoudReturnNothingIfStackIsEmpty

	| sentenceFinder |
	
	sentenceFinder _ SentenceFinderByPrefix newWith: stack.
	
	self assert: (sentenceFinder findByPrefix: 'Wint') isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'gabriel 9/18/2022 20:33:28'!
test02SentenceFinderShoudReturnNothingIfThereIsNoSentenceWithThatPrefix

	| sentenceFinder |
	
	stack push: 'Wint'.
	stack push: 'winter is coming'.
	
	sentenceFinder _ SentenceFinderByPrefix newWith: stack.
	
	self assert: (sentenceFinder findByPrefix: 'mama') isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'gabriel 9/18/2022 20:34:10'!
test03SentenceFinderShoudReturnNothingIfSentenceDoesNotMatchCaseForPrefix

	| sentenceFinder |
	
	stack push: 'Winter is here'.
	
	sentenceFinder _ SentenceFinderByPrefix newWith: stack.
	
	self assert: (sentenceFinder findByPrefix: 'wint') isEmpty.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'gabriel 9/18/2022 20:35:26'!
test04SentenceFinderShoudReturnSentenceIfItMatchesExactlyWithThePrefix

	| sentenceFinder foundSentences |
	
	self populateStack.
	
	sentenceFinder _ SentenceFinderByPrefix newWith: stack.
	
	foundSentences _ sentenceFinder findByPrefix: 'Wint'.
	
	self deny: foundSentences isEmpty.
	self assert: foundSentences size = 2.
	self assert: foundSentences first = 'Winter is here'.
	self assert: foundSentences second = 'Winter is coming'.! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'gabriel 9/18/2022 20:35:26'!
test05SentenceFinderShoudNotModifyStacksStructure

	| sentenceFinder |

	self populateStack.
	
	sentenceFinder _ SentenceFinderByPrefix newWith: stack.
	sentenceFinder findByPrefix: 'Wint'.
	
	self assert: stack size = 4.
	self assert: stack pop = 'Winter is here'.
	self assert: stack pop = 'The Winds of winter'.
	self assert: stack pop = 'winning is everything'.
	self assert: stack pop = 'Winter is coming'.
	
	! !


!SentenceFinderByPrefixTest methodsFor: 'setUps' stamp: 'gabriel 9/18/2022 20:35:26'!
populateStack

	stack push: 'Winter is coming'.
	stack push: 'winning is everything'.
	stack push: 'The Winds of winter'.
	stack push: 'Winter is here'! !

!SentenceFinderByPrefixTest methodsFor: 'setUps' stamp: 'gabriel 9/18/2022 20:32:57'!
setUp
	stack _ OOStack new.! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'stackTop'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 19:01:18'!
size

	^stackTop size! !

!OOStack methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 20:40:04'!
top
	^stackTop value! !


!OOStack methodsFor: 'initializing' stamp: 'gabriel 9/18/2022 19:01:18'!
initialize

	stackTop _ StackBase new.! !


!OOStack methodsFor: 'adding' stamp: 'gabriel 9/18/2022 19:05:11'!
push: anElement

	stackTop add: anElement into: self! !


!OOStack methodsFor: 'removing' stamp: 'gabriel 9/18/2022 19:12:36'!
pop
	^(stackTop removeFrom: self) value.
	! !


!OOStack methodsFor: 'operations - private' stamp: 'gabriel 9/18/2022 19:07:13'!
updateStackTopWith: aStackLayer 
	stackTop _ aStackLayer.! !


!OOStack methodsFor: 'testing' stamp: 'gabriel 9/18/2022 19:01:18'!
isEmpty

	^stackTop isEmpty ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'gabriel 9/18/2022 19:35:29'!
initializeWith: anOOStack 
	stack := anOOStack.! !


!SentenceFinderByPrefix methodsFor: 'operations' stamp: 'gabriel 9/18/2022 20:49:41'!
findByPrefix: aPrefix 
	
	^self obtainAllSentencesFromStack select: [:sentence | sentence beginsWith: aPrefix].
	
	
	! !


!SentenceFinderByPrefix methodsFor: 'operations - private' stamp: 'gabriel 9/18/2022 20:02:58'!
obtainAllSentencesFromStack 
	| stackSentences |
	
	stackSentences _ OrderedCollection new.
	
	[stack isEmpty] whileFalse: [stackSentences add: stack pop].
	
	self reconstructStackFrom: stackSentences.
	
	^stackSentences ! !

!SentenceFinderByPrefix methodsFor: 'operations - private' stamp: 'gabriel 9/18/2022 20:06:37'!
reconstructStackFrom: aSentenceCollection
	aSentenceCollection reverseDo: [:sentence | stack push: sentence].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'gabriel 9/18/2022 19:34:55'!
newWith: anOOStack

	^self new initializeWith: anOOStack! !


!classDefinition: #StackNode category: 'Stack-Exercise'!
Object subclass: #StackNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackNode methodsFor: 'adding' stamp: 'gabriel 9/18/2022 20:42:47'!
add: anElement into: anOOStack
	anOOStack updateStackTopWith: (StackLayer newFor: self withValue: anElement).! !


!StackNode methodsFor: 'testing' stamp: 'gabriel 9/18/2022 20:43:31'!
isEmpty
	self subclassResponsibility.! !


!StackNode methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 20:44:54'!
size
	self subclassResponsibility.! !

!StackNode methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 20:45:07'!
value
	self subclassResponsibility.! !


!StackNode methodsFor: 'removing' stamp: 'gabriel 9/18/2022 20:44:20'!
removeFrom: anOOStack
	self subclassResponsibility.
	
	! !


!classDefinition: #StackBase category: 'Stack-Exercise'!
StackNode subclass: #StackBase
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackBase methodsFor: 'testing' stamp: 'gabriel 9/18/2022 20:43:43'!
isEmpty
	^true! !


!StackBase methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 18:40:21'!
size
	^0.! !

!StackBase methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 18:46:15'!
value
	^self error: OOStack stackEmptyErrorDescription! !


!StackBase methodsFor: 'removing' stamp: 'gabriel 9/18/2022 19:14:51'!
removeFrom:  anOOStack
	^self error: OOStack stackEmptyErrorDescription! !


!classDefinition: #StackLayer category: 'Stack-Exercise'!
StackNode subclass: #StackLayer
	instanceVariableNames: 'next value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackLayer methodsFor: 'initialization' stamp: 'gabriel 9/18/2022 18:58:32'!
initializeFor: aStackNode withValue: anElement 
	next := aStackNode.
	value := anElement.! !


!StackLayer methodsFor: 'testing' stamp: 'gabriel 9/18/2022 18:27:19'!
isEmpty
	^false! !


!StackLayer methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 18:40:07'!
size
	^1 + next size! !

!StackLayer methodsFor: 'accessing' stamp: 'gabriel 9/18/2022 18:34:57'!
value
	^value.! !


!StackLayer methodsFor: 'removing' stamp: 'gabriel 9/18/2022 19:12:07'!
removeFrom: anOOStack
	anOOStack updateStackTopWith: next.
	
	^self.
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackLayer class' category: 'Stack-Exercise'!
StackLayer class
	instanceVariableNames: ''!

!StackLayer class methodsFor: 'as yet unclassified' stamp: 'gabriel 9/18/2022 18:22:36'!
newFor: aMmm 
	^self new initializeFor: aMmm ! !

!StackLayer class methodsFor: 'as yet unclassified' stamp: 'gabriel 9/18/2022 18:25:04'!
newFor: aMmm withValue: aString 
	^self new initializeFor: aMmm withValue: aString ! !
