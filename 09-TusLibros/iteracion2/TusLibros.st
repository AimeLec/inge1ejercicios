!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: self itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: self itemSellByTheStore.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 20:12:03'!
test09CannotAddANonPositiveQuantityOfAnItem

	| cart |
	
	cart _ self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 18:12:35'!
test10CannotAddANonIntegerQuantityOfAnItem

	| cart |
	
	cart _ self createCart.
	
	self 
		should: [cart add: 1.1 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !


!CartTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:48'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!CartTest methodsFor: 'support' stamp: 'ljm 10/31/2022 18:28:01'!
defaultCatalog
	
	^ Dictionary newFrom: { self itemSellByTheStore -> (1 * peso) }! !

!CartTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:44'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!CartTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:43'!
itemSellByTheStore
	
	^ 'validBook'! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 20:40:46'!
test01cannotCheckoutAnEmptyCart

	| cashier salesBook |

	cashier _ Cashier new.
	salesBook _ OrderedCollection new.

	self 
		should: [
			cashier
			checkout: (Cart acceptingItemsOf: Dictionary new)
			withCard: self notExpiredCard
			registeringOn: salesBook
			withMerchantProcessor: self testMerchantProcessor 
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Cashier emptyCartErrorMessage.
			self assert: salesBook isEmpty
		]! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 21:40:39'!
test02cashierReturnsCorrectAmountDueWhenCartHasASingleItem

	| cashier cart |

	cart _ self nonEmptyCart.
	cashier _ Cashier new.

	self
		assert: (123.50 * peso)
		equals: (
			cashier
				checkout: cart
				withCard: self notExpiredCard
				registeringOn: OrderedCollection new
				withMerchantProcessor: self testMerchantProcessor
			)
! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 20:39:02'!
test03cashierReturnsCorrectAmountDueWhenCartHasMultipleItems

	| cashier cart catalog priceForItem validItem anotherValidItem priceForAnotherItem |

	validItem _ 'valid item'.
	priceForItem _ 20 * peso.
	anotherValidItem _ 'another valid item'.
	priceForAnotherItem _ 5.5 * peso.
	catalog _ Dictionary newFrom: { validItem -> priceForItem. anotherValidItem -> priceForAnotherItem }.
	cart _ Cart acceptingItemsOf: catalog.
	cart add: 2 of: validItem.
	cart add: 2 of: anotherValidItem.

	cashier _ Cashier new.

	self
		assert: (priceForItem * 2) + (priceForAnotherItem  * 2)
		equals: (
			cashier
				checkout: cart
				withCard: self notExpiredCard
				registeringOn: OrderedCollection new
				withMerchantProcessor: self testMerchantProcessor
			)
! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 20:39:15'!
test04cashierAddsTheSaleOnSalesBookAfterCheckout

	| cart cashier salesBook |

	cart _ self nonEmptyCart.
	cashier _ Cashier new.
	salesBook _ OrderedCollection new.

	cashier
		checkout: cart
		withCard: self notExpiredCard
		registeringOn: salesBook
		withMerchantProcessor: self testMerchantProcessor.

	self deny: salesBook isEmpty! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 21:41:41'!
test05cannotCheckoutWithAnExpiredCardAndExternalMPItsNotCalled

	| cashier cart expiredCard salesBook |

	cart _ self nonEmptyCart.
	cashier _ Cashier new.
	expiredCard _ CreditCard withNumber: '0123456789012345' withName: 'any name' expiringOn: self lastMonth.
	salesBook _ OrderedCollection new.

	self 
		should: [
			cashier
				checkout: cart
				withCard: expiredCard
				registeringOn: salesBook
				withMerchantProcessor: MPTestDoubleForStolenCard new
			]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Cashier expiredCardErrorMessage.
			self assert: salesBook isEmpty
		]
! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 21:03:50'!
test06cannotDebitThroughMPWhenAmountDecimalPartIsLongerThan15Digits

	| cashier cart card validItem salesBook |

	validItem _ 'valid item'.
	cart _ Cart acceptingItemsOf: (Dictionary newFrom: { validItem -> (1000000000000000 * peso) }).
	cart add: 1 of: validItem.
	cashier _ Cashier new.
	card _ self notExpiredCard.
	salesBook _ OrderedCollection new.

	self 
		should: [
			cashier
				checkout: cart
				withCard: card
				registeringOn: salesBook
				withMerchantProcessor: self testMerchantProcessor 
			]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: MerchantProcessor invalidTransactionAmountErrorMessage.
			self assert: salesBook isEmpty
		]
! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 20:51:15'!
test07canDebitThroughMPWhenCartCostIsNotAFloatAsItIsConvertedToFloat

	| cashier cart card validItem salesBook |

	validItem _ 'valid item'.
	cart _ Cart acceptingItemsOf: (Dictionary newFrom: { validItem -> (1 * peso) }).
	cart add: 1 of: validItem.
	cashier _ Cashier new.
	card _ self notExpiredCard.
	salesBook  _ OrderedCollection new.

	cashier
		checkout: cart
		withCard: card
		registeringOn: salesBook
		withMerchantProcessor: self testMerchantProcessor .

	self deny: salesBook isEmpty! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 21:41:41'!
test08cannotDebitFromAnStolenCreditCard

	| cashier cart card salesBook |

	cart _ self nonEmptyCart.
	cashier _ Cashier new.
	card _ self notExpiredCard.
	salesBook _ OrderedCollection new.

	self 
		should: [
			cashier
				checkout: cart
				withCard: card
				registeringOn: salesBook
				withMerchantProcessor: MPTestDoubleForStolenCard new.
			]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: MerchantProcessor stolenCardErrorMessage.
			self assert: salesBook isEmpty
		]
! !

!CashierTest methodsFor: 'tests' stamp: 'ljm 11/2/2022 21:41:32'!
test09cannotDebitFromACreditCardThatHasNoEnoughFunds

	| cashier cart card salesBook |

	cart _ self nonEmptyCart.
	cashier _ Cashier new.
	card _ self notExpiredCard.
	salesBook _ OrderedCollection new.

	self 
		should: [
			cashier
				checkout: cart
				withCard: card
				registeringOn: salesBook
				withMerchantProcessor: MPTestDoubleForCardWithNoEnoughFunds new.
			]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: MerchantProcessor notEnoughFundsOnCardErrorMessage.
			self assert: salesBook isEmpty
		]
! !


!CashierTest methodsFor: 'support' stamp: 'ljm 10/31/2022 19:37:52'!
currentMonth

	^ GregorianMonthOfYear current! !

!CashierTest methodsFor: 'support' stamp: 'ljm 10/31/2022 19:24:34'!
defaultCatalog

	| catalog priceForItem validItem |

	validItem _ 'valid item'.
	priceForItem _ 20 * peso.
	^ catalog _ Dictionary newFrom: { validItem -> priceForItem }.! !

!CashierTest methodsFor: 'support' stamp: 'ljm 10/31/2022 19:37:52'!
lastMonth

	^ self currentMonth previous ! !

!CashierTest methodsFor: 'support' stamp: 'ljm 11/2/2022 21:40:23'!
nonEmptyCart

	| cart catalog priceForItem validItem |

	validItem _ 'valid item'.
	priceForItem _ 123.50 * peso.
	catalog _ Dictionary newFrom: { validItem -> priceForItem }.
	cart _ Cart acceptingItemsOf: catalog.
	cart add: 1 of: validItem.
	
	^ cart! !

!CashierTest methodsFor: 'support' stamp: 'ljm 11/2/2022 21:40:08'!
notExpiredCard

	^ CreditCard withNumber: '5400000000000001' withName: 'Pepe Sanchez' expiringOn: self thisMonth! !

!CashierTest methodsFor: 'support' stamp: 'ljm 11/2/2022 20:33:37'!
testMerchantProcessor

	^ MerchantProcessorTestDouble new! !

!CashierTest methodsFor: 'support' stamp: 'ljm 10/31/2022 19:37:52'!
thisMonth

	^ self currentMonth! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:38:58'!
test01aCardReturnsThatItsExpiredWhenItShould

	| card |

	card _ CreditCard withNumber: self validCardNumber withName: 'any name' expiringOn: self lastMonth.

	self assert: (card isExpired: Date current)! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:38:58'!
test02aCardReturnsThatItsNotExpiredWhenItShould

	| card |

	card _ CreditCard withNumber: self validCardNumber withName: 'any name' expiringOn: self currentMonth.

	self deny: (card isExpired: Date current)! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:38:58'!
test03cannotCreateACardWithLessThan16Characters

	| invalidNumber |

	invalidNumber _ ''.
	15 timesRepeat: [ invalidNumber _ invalidNumber,'1' ].

	self 
		should: [
			CreditCard withNumber: invalidNumber withName: 'any name' expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidCardNumberErrorMessage
		]! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:38:58'!
test04cannotCreateACardWithMoreThan16Characters

	| invalidNumber |

	invalidNumber _ ''.
	17 timesRepeat: [ invalidNumber _ invalidNumber,'1' ].

	self 
		should: [
			CreditCard withNumber: invalidNumber withName: 'any name' expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidCardNumberErrorMessage
		]! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:38:58'!
test05cannotCreateACardWithNonNumericCharacters

	| invalidNumber |

	invalidNumber _ ''.
	16 timesRepeat: [ invalidNumber _ invalidNumber,'a' ].

	self 
		should: [
			CreditCard withNumber: invalidNumber withName: 'any name' expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidCardNumberErrorMessage
		]! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:42:36'!
test06cannotCreateACardWithAnEmptyName

	self 
		should: [
			CreditCard withNumber: self validCardNumber withName: '' expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidClientNameErrorDescription 
		]! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:43:33'!
test07cannotCreateACardWithANameLongerThan30Characters

	| invalidClientName |

	invalidClientName _ ''.
	31 timesRepeat: [ invalidClientName _ invalidClientName,'a' ].

	self 
		should: [
			CreditCard withNumber: self validCardNumber withName: invalidClientName expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidClientNameErrorDescription 
		]! !

!CreditCardTest methodsFor: 'tests' stamp: 'ljm 10/31/2022 21:44:31'!
test08cannotCreateACardWithAllWhitespaces

	| invalidClientName |

	invalidClientName _ ''.
	20 timesRepeat: [ invalidClientName _ invalidClientName,' ' ].

	self 
		should: [
			CreditCard withNumber: self validCardNumber withName: invalidClientName expiringOn: self currentMonth
		]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: CreditCard invalidClientNameErrorDescription 
		]! !


!CreditCardTest methodsFor: 'support' stamp: 'ljm 10/31/2022 21:09:34'!
currentMonth

	^ GregorianMonthOfYear current! !

!CreditCardTest methodsFor: 'support' stamp: 'ljm 10/31/2022 21:09:25'!
lastMonth

	^ self currentMonth previous ! !

!CreditCardTest methodsFor: 'support' stamp: 'ljm 10/31/2022 21:33:11'!
validCardNumber

	| validNumber |

	validNumber _ ''.
	16 timesRepeat: [ validNumber _ validNumber,'1' ].
	
	^ validNumber! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'ljm 10/31/2022 18:27:22'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'ljm 10/31/2022 18:11:47'!
assertIsValidQuantity: aQuantity

	(aQuantity strictlyPositive and: [ aQuantity isInteger ] ) ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'ljm 10/31/2022 18:09:41'!
initializeAcceptingItemsOf: aCatalog

	catalog _ aCatalog.
	items _ Bag new.! !


!Cart methodsFor: 'queries' stamp: 'ljm 10/31/2022 18:38:56'!
cost

	^ items
		inject: 0
		into: [ :cost :item | cost + (catalog at: item) ]! !

!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'ljm 10/31/2022 18:10:00'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	items add: anItem withOccurrences: aQuantity ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout' stamp: 'ljm 11/2/2022 20:57:54'!
checkout: aCart withCard: aCard registeringOn: aSalesBook withMerchantProcessor: aMerchantProcessor   

	| amountDebited cartTotal |

	self validateCartNotEmpty: aCart.
	self validateCardNotExpired: aCard.

	cartTotal _ aCart cost.

	amountDebited _ aMerchantProcessor debit: cartTotal amount from: aCard.
	aSalesBook add: amountDebited.

	^ cartTotal! !

!Cashier methodsFor: 'checkout' stamp: 'ljm 10/31/2022 20:42:25'!
validateCardNotExpired: aCard.

	(aCard isExpired: Date current) ifTrue: [ ^ self error: self class expiredCardErrorMessage ]
! !

!Cashier methodsFor: 'checkout' stamp: 'ljm 11/2/2022 20:51:37'!
validateCartNotEmpty: aCart.

	aCart isEmpty ifTrue: [ ^ self error: self class emptyCartErrorMessage ]
! !

!Cashier methodsFor: 'checkout' stamp: 'ljm 11/2/2022 20:43:57'!
validateTransactionAmount: transactionAmount

	transactionAmount amount asFloat truncated printString size > 15
		ifTrue: [^ self error: MerchantProcessor invalidTransactionAmountErrorMessage ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'errors' stamp: 'ljm 10/31/2022 18:18:33'!
emptyCartErrorMessage

	^ 'Cannot checkout an empty cart'! !

!Cashier class methodsFor: 'errors' stamp: 'ljm 10/31/2022 19:31:24'!
expiredCardErrorMessage

	^ 'Cannot checkout with an expired card'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'gregorianMonthOfYear expiryDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'ljm 10/31/2022 21:39:39'!
withNumber: cardNumber withName: clientName initializeExpiringOn: expiryMonthAndYear

	expiryDate _ expiryMonthAndYear.! !


!CreditCard methodsFor: 'testing' stamp: 'ljm 10/31/2022 20:39:10'!
isExpired: aDate

	^ expiryDate < GregorianMonthOfYear current! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'ljm 10/31/2022 21:40:18'!
withNumber: cardNumber withName: clientName expiringOn: expiryMonthAndYear 

	self validateCardNumber: cardNumber.
	self validateClientName: clientName.

	^ self new withNumber: cardNumber withName: clientName initializeExpiringOn: expiryMonthAndYear ! !


!CreditCard class methodsFor: 'validations' stamp: 'ljm 10/31/2022 21:37:18'!
validateCardNumber: cardNumber

	((cardNumber size ~= 16) or: [cardNumber anySatisfy: [ :char | char isDigit not ]])
		ifTrue: [ self error: self invalidCardNumberErrorMessage ]! !

!CreditCard class methodsFor: 'validations' stamp: 'ljm 10/31/2022 21:46:29'!
validateClientName: aString 

	(((aString isEmpty) or: [ aString size > 30 ]) or: [ aString allSatisfy: [ :char | char = $ ]]) ifTrue: [ self error: self invalidClientNameErrorDescription ]! !


!CreditCard class methodsFor: 'errors' stamp: 'ljm 10/31/2022 21:34:58'!
invalidCardNumberErrorMessage

	^ 'Invalid card number'! !

!CreditCard class methodsFor: 'errors' stamp: 'ljm 10/31/2022 21:42:06'!
invalidClientNameErrorDescription

	^ 'Invalid client name'! !


!classDefinition: #MerchantProcessor category: 'TusLibros'!
Object subclass: #MerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessor methodsFor: 'debit' stamp: 'ljm 11/2/2022 20:58:24'!
debit: amountToDebit from: aCreditCard 

	self subclassResponsibility ! !


!MerchantProcessor methodsFor: 'debit-private' stamp: 'ljm 11/2/2022 21:14:31'!
debitWithExternalMerchantProcessor: amountToDebit from: aCreditCard 

	"Here the debit should be made through the external Merchant Processor"! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessor class' category: 'TusLibros'!
MerchantProcessor class
	instanceVariableNames: ''!

!MerchantProcessor class methodsFor: 'errors' stamp: 'ljm 11/2/2022 19:35:50'!
invalidTransactionAmountErrorMessage

	^ 'Transaction amount does not meet the requirements'! !

!MerchantProcessor class methodsFor: 'errors' stamp: 'ljm 11/2/2022 21:23:06'!
notEnoughFundsOnCardErrorMessage

	^ 'Not enough funds on card'! !

!MerchantProcessor class methodsFor: 'errors' stamp: 'ljm 11/2/2022 21:18:59'!
stolenCardErrorMessage

	^ 'Card is stolen'! !



!classDefinition: #MerchantProcessorTestDouble category: 'TusLibros'!
MerchantProcessor subclass: #MerchantProcessorTestDouble
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorTestDouble methodsFor: 'debit' stamp: 'ljm 11/2/2022 21:13:01'!
debit: amountToDebit from: aCreditCard 

	self validateTransactionAmount: amountToDebit.
	self debitWithExternalMerchantProcessor: amountToDebit from: aCreditCard.
	
	^ amountToDebit! !


!MerchantProcessorTestDouble methodsFor: 'validations' stamp: 'ljm 11/2/2022 20:55:57'!
validateTransactionAmount: amountToDebit

	amountToDebit amount asFloat truncated printString size > 15
		ifTrue: [^ self error: MerchantProcessor invalidTransactionAmountErrorMessage ]! !


!MerchantProcessorTestDouble methodsFor: 'debit - private' stamp: 'ljm 11/2/2022 21:13:46'!
debitWithExternalMerchantProcessor: amountToDebit from: aCreditCard 

	"Here the debit should be made through the external Merchant Processor"! !


!classDefinition: #MPTestDoubleForCardWithNoEnoughFunds category: 'TusLibros'!
MerchantProcessorTestDouble subclass: #MPTestDoubleForCardWithNoEnoughFunds
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MPTestDoubleForCardWithNoEnoughFunds methodsFor: 'debit - private' stamp: 'ljm 11/2/2022 21:21:04'!
debitWithExternalMerchantProcessor: amountToDebit from: aCreditCard 

	"We simulate an error on the Merchant Processor indicating that the card has no enough funds"
	
	^ self error: MerchantProcessor notEnoughFundsOnCardErrorMessage! !


!classDefinition: #MPTestDoubleForStolenCard category: 'TusLibros'!
MerchantProcessorTestDouble subclass: #MPTestDoubleForStolenCard
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MPTestDoubleForStolenCard methodsFor: 'debit - private' stamp: 'ljm 11/2/2022 21:17:03'!
debitWithExternalMerchantProcessor: amountToDebit from: aCreditCard 

	"We simulate an error on the Merchant Processor indicating that the card is stolen"
	
	^ self error: MerchantProcessor stolenCardErrorMessage! !
