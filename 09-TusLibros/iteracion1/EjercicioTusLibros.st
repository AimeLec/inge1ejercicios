!classDefinition: #CartTest category: 'EjercicioTusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioTusLibros'!

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:48:49'!
test01aNewCartShouldBeEmptyWhenCreated

	| cart catalog |

	catalog _ EditorialCatalog with: (Dictionary new).
	cart _ Cart newWithCatalog: catalog.
	
	self assert: cart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:52:43'!
test02aCartShouldNotBeEmptyAfterAddingABookToIt

	| cart validISBN catalog |

	validISBN _ 'an ISBN'.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { validISBN -> (5.99 * peso) }).
	
	cart _ Cart newWithCatalog: catalog.
	cart addBookWithISBN: validISBN.
	
	self deny: cart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:49:53'!
test03cannotAddABookThatDoesntBelongToCatalog

	| cart catalog |

	catalog _ EditorialCatalog with: (Dictionary new).
	cart _ Cart newWithCatalog: catalog.
	
	self should: [ cart addBookWithISBN: 'invalid ISBN' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :error |
			self assert: error messageText equals: Cart invalidBookErrorDescription.
			self assert: cart isEmpty
		]! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:52:53'!
test04aCartShouldListTheBookAddedToIt

	| cart validISBN catalog |

	validISBN _ 'an ISBN'.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { validISBN -> (5.99 * peso) }).
	
	cart _ Cart newWithCatalog: catalog.
	cart addBookWithISBN: validISBN.
	
	self
		assert: (Dictionary newFrom: { validISBN  -> 1 })
		equals: cart list.! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:53:05'!
test05aCartShouldListItsBooksWhenAllOfThemAreDifferent

	| cart validISBN anotherValidISBN catalog |

	validISBN _ 'an ISBN'.
	anotherValidISBN _ 'another ISBN'.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { validISBN -> (5.99 * peso). anotherValidISBN -> (14.99 * peso) }).
	
	cart _ Cart newWithCatalog: catalog.
	cart
		addBookWithISBN: anotherValidISBN;
		addBookWithISBN: validISBN.
	
	self
		assert: (Dictionary newFrom: { validISBN  -> 1. anotherValidISBN  -> 1 })
		equals: cart list.! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:53:13'!
test06aCartShouldListItsBooksWithItsQuantities

	| cart validISBN anotherValidISBN catalog |

	validISBN _ 'an ISBN'.
	anotherValidISBN _ 'another ISBN'.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { validISBN -> (5.99 * peso). anotherValidISBN -> (14.99 * peso) }).
	
	cart _ Cart newWithCatalog: catalog.
	cart
		addBookWithISBN: anotherValidISBN;
		addBookWithISBN: validISBN;
		addBookWithISBN: anotherValidISBN.
	
	self
		assert: (Dictionary newFrom: { validISBN  -> 1. anotherValidISBN  -> 2 })
		equals: cart list.! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:47:38'!
test07aCartHasCostZeroWhenEmpty

	| cart |

	cart _ Cart newWithCatalog: (Dictionary new).
	
	self assert: 0 equals: cart cost.! !

!CartTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:53:44'!
test08aCartReturnsTheCorrectCostWhenItHasBooks

	| cart anISBN anotherISBN catalog anotherPrice price |

	anISBN _ 'an ISBN'.
	price _ 14.99 * peso.
	anotherISBN _ 'another ISBN'.
	anotherPrice _ 5.99 * peso.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { anISBN -> price. anotherISBN -> anotherPrice }).
	
	cart _ Cart newWithCatalog: catalog.
	cart
		addBookWithISBN: anotherISBN;
		addBookWithISBN: anISBN;
		addBookWithISBN: anotherISBN.
	
	self assert: (2 * 5.99 + 14.99) * peso equals: cart cost.! !


!classDefinition: #EditorialCatalogTest category: 'EjercicioTusLibros'!
TestCase subclass: #EditorialCatalogTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioTusLibros'!

!EditorialCatalogTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:10:19'!
test01aCatalogReturnsFalseWhenItDoesntHaveTheRequestedISBN

	self deny: (EditorialCatalog new hasBookWith: 'an ISBN')! !

!EditorialCatalogTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:54:05'!
test02aCatalogReturnsTrueWhenItHasTheRequestedISBNs

	| anISBN catalog anotherISBN |

	anISBN _ 'an ISBN'.
	anotherISBN _ 'another ISBN'.
	catalog _ EditorialCatalog with: (Dictionary newFrom: { anISBN -> (14.99 * peso). anotherISBN -> (5.99 * peso) }).
	
	self assert: (catalog hasBookWith: anISBN).
	self assert: (catalog hasBookWith: anotherISBN)! !

!EditorialCatalogTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:23:28'!
test03aCatalogShouldRaiseErrorWhenAskedForPriceOfAnISBNThatItDoesntHave

	| catalog |

	catalog _ EditorialCatalog new.
	
	self should: [ catalog priceFor: 'any ISBN' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :error |
			self assert: error messageText equals: EditorialCatalog invalidISBNErrorDescription
		]! !

!EditorialCatalogTest methodsFor: 'tests' stamp: 'ljm 10/30/2022 15:54:18'!
test04aCatalogReturnsTheCorrectPriceForAnISBNThatItHasRegistered

	| catalog anISBN price |

	anISBN _ 'an ISBN'.
	price _ 14.99 * peso .
	catalog _ EditorialCatalog with: (Dictionary newFrom: { anISBN -> price }).
	
	self assert: price equals: (catalog priceFor: anISBN).! !


!classDefinition: #Cart category: 'EjercicioTusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'hasBook catalog books'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioTusLibros'!

!Cart methodsFor: 'testing' stamp: 'ljm 10/30/2022 14:59:08'!
isEmpty

	^ books size = 0! !


!Cart methodsFor: 'modifying' stamp: 'ljm 10/30/2022 15:40:25'!
addBookWithISBN: anISBN

	(catalog hasBookWith: anISBN)
		ifTrue: [ books add: anISBN. ]
		ifFalse: [ self error: self class invalidBookErrorDescription ]
	! !


!Cart methodsFor: 'initialization' stamp: 'ljm 10/30/2022 14:59:38'!
initializeNewWithCatalog: aCatalog 

	catalog _ aCatalog.
	books _ OrderedCollection new.
! !


!Cart methodsFor: 'accessing' stamp: 'ljm 10/30/2022 15:46:06'!
cost

	^ books
		inject: 0
		into: [ :cost :book |  cost + (catalog priceFor: book) ].! !

!Cart methodsFor: 'accessing' stamp: 'ljm 10/30/2022 14:55:29'!
list

	| dictionary |
	
	dictionary _ Dictionary new.
	
	books do: [ :aBook |
		(dictionary includesKey: aBook)
			ifTrue: [ | currentQuantity |
				currentQuantity _ dictionary at: aBook.
				dictionary add: aBook -> (currentQuantity + 1)
			]
			ifFalse: [ dictionary add: aBook -> 1 ]
	].
	
	^ dictionary! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'EjercicioTusLibros'!
Cart class
	instanceVariableNames: ''!


!Cart class methodsFor: 'instance creation' stamp: 'ljm 10/27/2022 21:01:31'!
newWithCatalog: aCatalog 

	^ self new initializeNewWithCatalog: aCatalog ! !


!Cart class methodsFor: 'errors' stamp: 'ljm 10/27/2022 21:05:29'!
invalidBookErrorDescription

	^ 'Cannot add a book not present in the catalog'! !


!classDefinition: #EditorialCatalog category: 'EjercicioTusLibros'!
Object subclass: #EditorialCatalog
	instanceVariableNames: 'iSBN books anotherISBN_CHANGE_ME'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioTusLibros'!

!EditorialCatalog methodsFor: 'testing' stamp: 'ljm 10/30/2022 15:33:54'!
hasBookWith: anISBN

	^ books includesKey: anISBN ! !


!EditorialCatalog methodsFor: 'initialization' stamp: 'ljm 10/30/2022 15:34:22'!
initialize

	books _ Dictionary new.! !

!EditorialCatalog methodsFor: 'initialization' stamp: 'ljm 10/30/2022 15:31:45'!
initializeWith: aDictionaryOfISBNsAndPrices  

	books _ aDictionaryOfISBNsAndPrices! !


!EditorialCatalog methodsFor: 'accessing' stamp: 'ljm 10/30/2022 15:33:08'!
priceFor: anISBN 

	^ books at: anISBN ifAbsent: [ ^ self error: self class invalidISBNErrorDescription ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EditorialCatalog class' category: 'EjercicioTusLibros'!
EditorialCatalog class
	instanceVariableNames: ''!

!EditorialCatalog class methodsFor: 'instance creation' stamp: 'ljm 10/30/2022 15:30:42'!
with: aDictionaryOfISBNsAndPrices 

	^self new initializeWith: aDictionaryOfISBNsAndPrices ! !

!EditorialCatalog class methodsFor: 'instance creation' stamp: 'ljm 10/30/2022 15:17:48'!
with: anISBN with: anotherISBN 
	^self new initializeWith: anISBN ! !


!EditorialCatalog class methodsFor: 'errors' stamp: 'ljm 10/30/2022 15:24:35'!
invalidISBNErrorDescription

	^ 'The ISBN is not present in the catalog'! !
