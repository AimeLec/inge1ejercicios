!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 16:38:19'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook  |
	
	customerBook := CustomerBook new.
	
	self assertThat: [	customerBook addCustomerNamed: 'John Lennon'.] takesLessThan: 50 * millisecond.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 16:39:20'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	
	self assertThat: [customerBook removeCustomerNamed: paulMcCartney.] takesLessThan: 100 * millisecond.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 16:47:39'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.
	
	self
		assertThat: [customerBook addCustomerNamed: ''.]
		on: Error
		satisfies: [ :anError |
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty.
		]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 16:50:37'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self
		assertThat: [customerBook removeCustomerNamed: 'Paul McCartney'.]
		on: NotFound
		satisfies: [ :anError |
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon).
		]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 17:13:31'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	self addAndSuspendCustomerTo: customerBook named: paulMcCartney.
	
	self
		assertFor: customerBook
		expectedActiveCustomers: 0
		expectedSuspendedCustomers: 	1.
	
	self assert: (customerBook includesCustomerNamed: paulMcCartney).! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 17:13:36'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	self addAndSuspendCustomerTo: customerBook named: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self
		assertFor: customerBook
		expectedActiveCustomers: 0
		expectedSuspendedCustomers: 	0.

	self deny: (customerBook includesCustomerNamed: paulMcCartney).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 18:33:59'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self
		assertCannotSuspendFrom: customerBook
		customerToSuspend: 'George Harrison'
		andKeepInBook: johnLennon.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ljm 9/6/2022 18:33:59'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	self addAndSuspendCustomerTo: customerBook named: johnLennon.
	
	self
		assertCannotSuspendFrom: customerBook
		customerToSuspend: johnLennon
		andKeepInBook: johnLennon.
! !


!CustomerBookTest methodsFor: 'assertions' stamp: 'ljm 9/6/2022 18:34:19'!
assertCannotSuspendFrom: aCustomerBook customerToSuspend: aCustomerName andKeepInBook: anotherCustomerName

	self
		assertThat: [aCustomerBook suspendCustomerNamed: aCustomerName.]
		on: CantSuspend
		satisfies: [ :anError |
			self assert: aCustomerBook numberOfCustomers = 1.
			self assert: (aCustomerBook includesCustomerNamed: anotherCustomerName)
		]! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'ljm 9/6/2022 17:12:18'!
assertFor: aCustomerBook expectedActiveCustomers: activeCustomersQuantity expectedSuspendedCustomers: suspendedCustomersQuantity

	self assert: activeCustomersQuantity equals: aCustomerBook numberOfActiveCustomers.
	self assert: suspendedCustomersQuantity equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: ( activeCustomersQuantity + suspendedCustomersQuantity ) equals: aCustomerBook numberOfCustomers.
! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'ljm 9/6/2022 16:49:19'!
assertThat: aBlock on: anError satisfies: aBlockToAssert

	[ aBlock value. self fail ]
		on: anError 
		do: aBlockToAssert
! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'ljm 9/6/2022 16:37:30'!
assertThat: aBlock takesLessThan: boundTime

	| measuredTime |

	measuredTime _ self measureTimeOf: aBlock value.
	
	self assert: measuredTime < boundTime
	! !


!CustomerBookTest methodsFor: 'time measurement' stamp: 'ljm 9/6/2022 16:32:28'!
measureTimeOf: aBlock

	| millisecondsAfterRunning millisecondsBeforeRunning |

	millisecondsBeforeRunning _ Time millisecondClockValue * millisecond.
	aBlock value.
	millisecondsAfterRunning _ Time millisecondClockValue * millisecond.

	^ millisecondsAfterRunning-millisecondsBeforeRunning! !


!CustomerBookTest methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:32:44'!
addAndSuspendCustomerTo: aCustomerBook named: aCustomerName

	aCustomerBook addCustomerNamed: aCustomerName.
	aCustomerBook suspendCustomerNamed: aCustomerName.

	^ aCustomerBook! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'ljm 9/6/2022 18:32:30'!
includesCustomerNamed: aCustomerName

	^self includesCustomer: aCustomerName ! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'errors' stamp: 'ljm 9/6/2022 18:21:48'!
raiseErrorMessage: errorMessage
	self error: (self class perform: errorMessage)! !

!CustomerBook methodsFor: 'errors' stamp: 'ljm 9/6/2022 18:15:26'!
signalCustomerAlreadyExists 

	self raiseErrorMessage: #customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'errors' stamp: 'ljm 9/6/2022 18:22:22'!
signalCustomerNameCannotBeEmpty 

	self raiseErrorMessage: #customerCanNotBeEmptyErrorMessage.! !


!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:32:20'!
addCustomerNamed: aCustomerName

	aCustomerName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomer: aCustomerName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aCustomerName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:25:14'!
includesCustomer: aCustomerName

	^ (active includes: aCustomerName) or: [suspended includes: aCustomerName].! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 17:36:53'!
numberOfCustomers
	
	^ self numberOfActiveCustomers + self numberOfSuspendedCustomers ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:31:37'!
remove: aCustomerName from: aList andIfFoundDo: aBlock
	1 to: aList size do: 
	[ :index |
		aCustomerName = (aList at: index)
			ifTrue: [
				aList removeAt: index.
				^ aBlock value
			] 
	].! !

!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:31:47'!
removeCustomerNamed: aCustomerName 

	self remove: aCustomerName from: active andIfFoundDo: [^ aCustomerName].
	self remove: aCustomerName from: suspended andIfFoundDo: [^ aCustomerName].

	^ NotFound signal.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'ljm 9/6/2022 18:32:08'!
suspendCustomerNamed: aCustomerName 
	
	(active includes: aCustomerName) ifFalse: [^CantSuspend signal].
	
	active remove: aCustomerName.
	
	suspended add: aCustomerName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:12'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
