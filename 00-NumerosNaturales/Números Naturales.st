!classDefinition: #I category: 'N�meros Naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'N�meros Naturales'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'next' stamp: 'ARM 8/25/2022 20:43:03'!
next
	^II! !


!I class methodsFor: 'order' stamp: 'ljm 8/27/2022 11:18:21'!
< aComparator
	
	aComparator = I ifTrue:[^ false].
	^ true! !

!I class methodsFor: 'order' stamp: 'ljm 8/27/2022 11:18:29'!
<= aComparator
	^ false! !


!I class methodsFor: 'operations' stamp: 'ljm 8/27/2022 10:26:11'!
* multiplicand
	^ multiplicand ! !

!I class methodsFor: 'operations' stamp: 'ARM 8/25/2022 21:12:30'!
+ anAdder

	^anAdder next! !

!I class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:50:40'!
- subtrahend

	self error: 'subtrahend can not be greater than minuend'! !

!I class methodsFor: 'operations' stamp: 'ljm 8/27/2022 10:47:32'!
/ denominator
	
	denominator  = I ifTrue:[ ^ self ].
	
	self error: 'Numerator cannot be greater than denominator'! !

!I class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:53:09'!
canNotDivideByBiggerNumberErrorDescription
	^'Numerator cannot be greater than denominator'! !

!I class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:52:29'!
negativeNumbersNotSupportedErrorDescription
	^'subtrahend can not be greater than minuend'! !

!I class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:12:02'!
substractFrom: minuend
	^minuend previous! !


!classDefinition: #II category: 'N�meros Naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'N�meros Naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'next & previous' stamp: 'gabriel 8/29/2022 23:45:41'!
nameOfNext
	(self name endsWith: 'CMXCIX') ifTrue: [^ (self name withoutSuffix: 'CMXCIX') , 'M'].
	(self name endsWith: 'DCCCXCIX') ifTrue: [^ (self name withoutSuffix: 'DCCCXCIX') , 'CM'].
	(self name endsWith: 'CCCXCIX') ifTrue: [^ (self name withoutSuffix: 'CCCXCIX') , 'CD'].
	(self name endsWith: 'XCIX') ifTrue: [^ (self name withoutSuffix: 'XCIX') , 'C'].
	(self name endsWith: 'LXXXIX') ifTrue: [^ (self name withoutSuffix: 'LXXXIX') , 'XC'].
	(self name endsWith: 'XLIX') ifTrue: [^ (self name withoutSuffix: 'XLIX') , 'L'].
	(self name endsWith: 'XXXIX') ifTrue: [^ (self name withoutSuffix: 'XXXIX') , 'XL'].
	(self name endsWith: 'VIII') ifTrue: [^ (self name withoutSuffix: 'VIII') , 'IX'].
	(self name endsWith: 'IX') ifTrue: [^ (self name withoutSuffix: 'IX') , 'X'].
	(self name endsWith: 'III') ifTrue: [^ (self name withoutSuffix: 'III') , 'IV'].
	(self name endsWith: 'IV') ifTrue: [^ (self name withoutSuffix: 'IV') , 'V'].
	^ self name , 'I'! !

!II class methodsFor: 'next & previous' stamp: 'gabriel 8/29/2022 23:17:53'!
next
	next ifNil:[
		next _ II createChildNamed: self nameOfNext.
		next previous: self.
		].
	^next! !

!II class methodsFor: 'next & previous' stamp: 'ARM 8/25/2022 21:21:12'!
previous

	^previous! !

!II class methodsFor: 'next & previous' stamp: 'ARM 8/25/2022 21:25:57'!
previous: aNumber 

	previous := aNumber! !


!II class methodsFor: 'remove all next' stamp: 'ARM 8/25/2022 21:37:56'!
removeAllNext

	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !


!II class methodsFor: 'order' stamp: 'ljm 8/27/2022 11:18:39'!
< aComparator

	aComparator = I ifTrue: [^ false].
	
	^ self previous < aComparator previous! !

!II class methodsFor: 'order' stamp: 'ljm 8/27/2022 11:18:48'!
<= aComparator

	aComparator = I ifTrue: [^ false].
	
	^ self previous <= aComparator previous! !


!II class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:16:54'!
* multiplicand
	^self previous * multiplicand + multiplicand! !

!II class methodsFor: 'operations' stamp: 'ARM 8/25/2022 21:19:09'!
+ anAdder 

	^self previous + anAdder next! !

!II class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:12:55'!
- subtrahend 
	^subtrahend substractFrom: self! !

!II class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:46:37'!
/ denominator

	self < denominator ifTrue:[ self error: 'Numerator cannot be greater than denominator' ].
	self = denominator ifTrue:[ ^ I ].
	self - denominator < denominator ifTrue: [^ I ].
	
	^ (self - denominator / denominator) next
	! !

!II class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:53:16'!
canNotDivideByBiggerNumberErrorDescription
	^'Numerator cannot be greater than denominator'
	! !

!II class methodsFor: 'operations' stamp: 'gabriel 8/29/2022 23:10:59'!
substractFrom: minuend
	^minuend previous - self previous
	! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'gabriel 9/4/2022 15:47:45'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'N�meros Naturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'N�meros Naturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'gabriel 9/4/2022 15:47:45'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IV.
	previous := II.! !


!classDefinition: #IV category: 'N�meros Naturales'!
II subclass: #IV
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IV class' category: 'N�meros Naturales'!
IV class
	instanceVariableNames: ''!

!IV class methodsFor: '--** private fileout/in **--' stamp: 'gabriel 9/4/2022 15:47:46'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IV initializeAfterFileIn!