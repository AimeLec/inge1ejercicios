!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #RoverLoggerTest category: 'MarsRover-WithHeading'!
TestCase subclass: #RoverLoggerTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverLoggerTest methodsFor: 'headings' stamp: 'ljm 10/24/2022 20:05:48'!
east

	^ MarsRoverHeadingEast ! !

!RoverLoggerTest methodsFor: 'headings' stamp: 'ljm 10/24/2022 20:05:36'!
north

	^ MarsRoverHeadingNorth ! !

!RoverLoggerTest methodsFor: 'headings' stamp: 'ljm 10/24/2022 20:05:42'!
south

	^ MarsRoverHeadingSouth ! !

!RoverLoggerTest methodsFor: 'headings' stamp: 'ljm 10/24/2022 20:05:54'!
west

	^ MarsRoverHeadingWest ! !


!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:20:04'!
test01ProcessingNoCommandsShouldNotLogAnythingInRoverPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverPositionLogger for: rover .
	
	rover process: ''.
	
	self assert: logger logs isEmpty.! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:22:03'!
test02MovingNorthShouldLogTheChangeInsideTheRoverPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverPositionLogger for: rover .
	
	rover process: 'f'.
	
	self assert: 1 equals: logger logs size.
	self assert: '1@2' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:22:33'!
test03MovingSouthShouldLogTheChangeInsideTheRoverPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverPositionLogger for: rover .
	
	rover process: 'b'.
	
	self assert: 1 equals: logger logs size.
	self assert: '1@0' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:25:20'!
test04MovingEastShouldLogTheChangeInsideTheRoverPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self east.
	logger _ RoverPositionLogger for: rover .
	
	rover process: 'f'.
	
	self assert: 1 equals: logger logs size.
	self assert: '2@1' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:25:32'!
test05MovingWestShouldLogTheChangeInsideTheRoverPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self east.
	logger _ RoverPositionLogger for: rover .
	
	rover process: 'b'.
	
	self assert: 1 equals: logger logs size.
	self assert: '0@1' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:26:10'!
test06MovingIntoSeveralDirectionsShouldLogAllMovementsInPositionLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverPositionLogger for: rover .
	
	rover process: 'fflfblf'.
	
	self assert: 5 equals: logger logs size.
	self assert: '1@2' equals: (logger logs at: 1).
	self assert: '1@3' equals: (logger logs at: 2).
	self assert: '0@3' equals: (logger logs at: 3).
	self assert: '1@3' equals: (logger logs at: 4).
	self assert: '1@2' equals: (logger logs at: 5).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:26:34'!
test07ProcessingNoCommandsShouldNotLogAnythingInHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: ''.
	
	self assert: logger logs isEmpty.! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:27:19'!
test08RotatingToEastShouldLogTheChangeInsideTheHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: 'r'.
	
	self assert: 1 equals: logger logs size.
	self assert: 'East' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:27:33'!
test09RotatingToWestShouldLogTheChangeInsideTheHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: 'l'.
	
	self assert: 1 equals: logger logs size.
	self assert: 'West' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:27:54'!
test10RotatingToNorthShouldLogTheChangeInsideTheHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self east.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: 'l'.
	
	self assert: 1 equals: logger logs size.
	self assert: 'North' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:28:10'!
test11RotatingToSouthShouldLogTheChangeInsideTheHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self east.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: 'r'.
	
	self assert: 1 equals: logger logs size.
	self assert: 'South' equals: (logger logs at: 1).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:28:48'!
test12RotatingSeveralTimesShouldLogAllMovementsInHeadingLogger

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self east.
	logger _ RoverHeadingLogger for: rover.
	
	rover process: 'rrrr'.
	
	self assert: 4 equals: logger logs size.
	self assert: 'South' equals: (logger logs at: 1).
	self assert: 'West' equals: (logger logs at: 2).
	self assert: 'North' equals: (logger logs at: 3).
	self assert: 'East' equals: (logger logs at: 4).! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:29:27'!
test13ProcessingNoCommandsShouldNotLogAnythingInRoverPositionAndHeadingLogger
	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingAndPositionLogger for: rover .
	
	rover process: ''.
	
	self assert: logger logs isEmpty.! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:30:20'!
test14HeadingAndPositionLoggerShouldLogPositionMovements

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingAndPositionLogger for: rover .
	
	rover process: 'fb'.
	
	self assert: 2 equals: logger logs size.
	self assert: '1@2' equals: (logger logs at: 1).
	self assert: '1@1' equals: (logger logs at: 2).
	! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:31:02'!
test15HeadingAndPositionLoggerShouldLogBothPositionsAndHeadingChanges

	| logger rover |

	rover _ MarsRover at: 1@1 heading: self north.
	logger _ RoverHeadingAndPositionLogger for: rover .
	
	rover process: 'lbfr'.
	
	self assert: 4 equals: logger logs size.
	self assert: 'West' equals: (logger logs at: 1).
	self assert: '2@1' equals: (logger logs at: 2).
	self assert: '1@1' equals: (logger logs at: 3).
	self assert: 'North' equals: (logger logs at: 4).
	! !

!RoverLoggerTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:31:27'!
test16MarsRoverShouldBeAbleToHandleMultipleLoggers

	| positionLogger rover headingLogger |

	rover _ MarsRover at: 1@1 heading: self north.
	positionLogger _ RoverPositionLogger for: rover.
	headingLogger _ RoverHeadingLogger for: rover.
	
	rover process: 'lbfr'.
	
	self assert: 2 equals: positionLogger logs size.
	self assert: '2@1' equals: (positionLogger logs at: 1).
	self assert: '1@1' equals: (positionLogger logs at: 2).

	self assert: 2 equals: headingLogger logs size.
	self assert: 'West' equals: (headingLogger logs at: 1).
	self assert: 'North' equals: (headingLogger logs at: 2).	! !


!classDefinition: #RoverWindowTest category: 'MarsRover-WithHeading'!
TestCase subclass: #RoverWindowTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:34:08'!
test01PositionWindowShouldDisplayInitialPositionWhenNoCommandsAreExecuted

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverPositionWindow for: rover.

	rover process: ''.

	self assert: '1@1' equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:34:36'!
test02PositionWindowShouldDisplayCurrentPositionAtExecutingOneCommand

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverPositionWindow for: rover.

	rover process: 'f'.

	self assert: '2@1' equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:35:24'!
test03PositionWindowShouldDisplayCurrentPositionAtExecutingSeveralCommands

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverPositionWindow for: rover.

	rover process: 'fflf'.

	self assert: '3@2' equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:37:32'!
test04HeadingWindowShouldDisplayInitialHeadingDirectionWhenNoCommandsAreExecuted

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingWindow for: rover.

	rover process: ''.

	self assert: #East equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:38:28'!
test05HeadingWindowShouldDisplayCurrentHeadingDirectionAtExecutingOneCommand

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingWindow for: rover.

	rover process: 'r'.

	self assert: #South equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:38:36'!
test06HeadingWindowShouldDisplayCurrentHeadingDirectionAtExecutingSeveralCommands

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingWindow for: rover.

	rover process: 'lrrlr'.

	self assert: #South equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:41:07'!
test07PositionAndHeadingWindowShouldDisplayBothInitialPropertiesAtExecutingNoCommands

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingAndPositionWindow for: rover.

	rover process: ''.

	self assert: '1@1 - East' equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:42:19'!
test08PositionAndHeadingWindowShouldDisplayCurrentPositionWhenExecutingMovementCommands

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingAndPositionWindow for: rover.

	rover process: 'fffb'.

	self assert: '3@1 - East' equals: window show.! !

!RoverWindowTest methodsFor: 'tests' stamp: 'gl 10/26/2022 22:42:46'!
test09PositionAndHeadingWindowShouldDisplayBothCurrentPropertiesWhenExecutingCommands

	| window rover |

	rover _ MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	window _ RoverHeadingAndPositionWindow for: rover.

	rover process: 'lffrbr'.

	self assert: '0@3 - South' equals: window show.! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head observers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'ljm 10/25/2022 17:29:45'!
initializeAt: aPosition heading: aHeadingType

	position _ aPosition.
	head _ aHeadingType for: self.
	
	observers _ OrderedCollection new.! !


!MarsRover methodsFor: 'heading' stamp: 'ljm 10/25/2022 17:28:50'!
headEast
	
	head _ MarsRoverHeadingEast for: self.
	self notifyObservers.! !

!MarsRover methodsFor: 'heading' stamp: 'ljm 10/25/2022 17:28:52'!
headNorth
	
	head _ MarsRoverHeadingNorth for: self.
	self notifyObservers.! !

!MarsRover methodsFor: 'heading' stamp: 'ljm 10/25/2022 17:28:55'!
headSouth
	
	head _ MarsRoverHeadingSouth for: self.
	self notifyObservers.! !

!MarsRover methodsFor: 'heading' stamp: 'ljm 10/25/2022 17:28:57'!
headWest
	
	head _ MarsRoverHeadingWest for: self.
	self notifyObservers.! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'ljm 10/25/2022 17:29:14'!
moveEast
	
	position _ position + (1@0).
	self notifyObservers.! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	head moveForward! !

!MarsRover methodsFor: 'moving' stamp: 'ljm 10/25/2022 17:28:26'!
moveNorth
	
	position _ position + (0@1).
	self notifyObservers.! !

!MarsRover methodsFor: 'moving' stamp: 'ljm 10/25/2022 17:29:10'!
moveSouth
	
	position _ position + (0@-1).
	self notifyObservers.! !

!MarsRover methodsFor: 'moving' stamp: 'ljm 10/25/2022 17:28:26'!
moveWest
	
	position _ position + (-1@0).
	self notifyObservers.! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'loggers' stamp: 'ljm 10/25/2022 17:28:37'!
notifyObservers

	observers do: [ :anObserver | anObserver update ]
		! !

!MarsRover methodsFor: 'loggers' stamp: 'ljm 10/25/2022 18:23:41'!
setObserver: anObserver

	observers addLast: anObserver.! !


!MarsRover methodsFor: 'get state' stamp: 'ljm 10/25/2022 17:33:43'!
getHeading

	^ head asCardinal ! !

!MarsRover methodsFor: 'get state' stamp: 'ljm 10/25/2022 17:32:59'!
getPosition

	^ position! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'ljm 10/24/2022 21:33:11'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !


!MarsRoverHeading methodsFor: 'accessing' stamp: 'ljm 10/24/2022 21:19:06'!
asCardinal

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'accessing' stamp: 'ljm 10/24/2022 21:19:19'!
asCardinal

	^ #East! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'accessing' stamp: 'ljm 10/24/2022 21:19:40'!
asCardinal

	^ #North! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!MarsRoverHeadingSouth methodsFor: 'accessing' stamp: 'ljm 10/24/2022 21:19:50'!
asCardinal

	^ #South! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'accessing' stamp: 'ljm 10/24/2022 21:19:58'!
asCardinal

	^ #West! !


!classDefinition: #RoverLogger category: 'MarsRover-WithHeading'!
Object subclass: #RoverLogger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverLogger methodsFor: 'initialization' stamp: 'ljm 10/24/2022 21:36:52'!
initializeFor: aMarsRover 

	self subclassResponsibility ! !


!RoverLogger methodsFor: 'update' stamp: 'ljm 10/25/2022 17:27:53'!
update

	self subclassResponsibility ! !


!RoverLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:39:04'!
logHeading

	self subclassResponsibility ! !

!RoverLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:37:59'!
logPosition

	self subclassResponsibility ! !

!RoverLogger methodsFor: 'logs' stamp: 'ljm 10/24/2022 21:37:26'!
logs

	self subclassResponsibility ! !


!classDefinition: #RoverHeadingAndPositionLogger category: 'MarsRover-WithHeading'!
RoverLogger subclass: #RoverHeadingAndPositionLogger
	instanceVariableNames: 'logs rover observedPosition observedHeading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverHeadingAndPositionLogger methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	logs _ OrderedCollection new.
	rover _ aMarsRover.

	rover setObserver: self.
	observedPosition _ rover getPosition.
	observedHeading _ rover getHeading.
	! !


!RoverHeadingAndPositionLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:46:32'!
logHeading

	logs addLast: observedHeading ! !

!RoverHeadingAndPositionLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:37:59'!
logPosition

	logs addLast: observedPosition printString ! !

!RoverHeadingAndPositionLogger methodsFor: 'logs' stamp: 'ljm 10/24/2022 21:27:35'!
logs

	^ logs! !


!RoverHeadingAndPositionLogger methodsFor: 'update' stamp: 'ljm 10/25/2022 17:42:43'!
update

	| observerHeading observerPosition |

	observerPosition _ rover getPosition.
	observerHeading _ rover getHeading.
	
	(observerHeading ~= observedHeading) ifTrue: [
		observedHeading  _ observerHeading.
		self logHeading.	
	].

	(observerPosition ~= observedPosition) ifTrue: [
		observedPosition  _ observerPosition.
		self logPosition.	
	]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverHeadingAndPositionLogger class' category: 'MarsRover-WithHeading'!
RoverHeadingAndPositionLogger class
	instanceVariableNames: ''!

!RoverHeadingAndPositionLogger class methodsFor: 'instance creation' stamp: 'ljm 10/24/2022 21:23:43'!
for: aMarsRover 

	^ self new initializeFor: aMarsRover ! !


!classDefinition: #RoverHeadingLogger category: 'MarsRover-WithHeading'!
RoverLogger subclass: #RoverHeadingLogger
	instanceVariableNames: 'logs rover observedHeading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverHeadingLogger methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	logs _ OrderedCollection new.	
	rover _ aMarsRover.

	rover setObserver: self.
	observedHeading _ rover getHeading.! !


!RoverHeadingLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:39:35'!
logHeading

	logs addLast: observedHeading.! !

!RoverHeadingLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:37:59'!
logPosition
! !

!RoverHeadingLogger methodsFor: 'logs' stamp: 'ljm 10/24/2022 21:06:26'!
logs

	^ logs! !


!RoverHeadingLogger methodsFor: 'update' stamp: 'ljm 10/25/2022 17:43:42'!
update

	| observerHeading |

	observerHeading _ rover getHeading.
	
	(observerHeading ~= observedHeading) ifTrue: [
		observedHeading  _ observerHeading.
		self logHeading.	
	].
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverHeadingLogger class' category: 'MarsRover-WithHeading'!
RoverHeadingLogger class
	instanceVariableNames: ''!

!RoverHeadingLogger class methodsFor: 'instance creation' stamp: 'ljm 10/24/2022 20:59:35'!
for: aMarsRover 

	^ self new initializeFor: aMarsRover ! !


!classDefinition: #RoverPositionLogger category: 'MarsRover-WithHeading'!
RoverLogger subclass: #RoverPositionLogger
	instanceVariableNames: 'logs rover observedPosition'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverPositionLogger methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	logs _ OrderedCollection new.
	rover _ aMarsRover.

	rover setObserver: self.
	observedPosition _ rover getPosition.! !


!RoverPositionLogger methodsFor: 'logs' stamp: 'ljm 10/25/2022 17:37:59'!
logPosition

	logs addLast: observedPosition printString ! !

!RoverPositionLogger methodsFor: 'logs' stamp: 'ljm 10/24/2022 20:20:43'!
logs

	^ logs! !


!RoverPositionLogger methodsFor: 'update' stamp: 'ljm 10/25/2022 17:44:41'!
update

	| observerPosition |

	observerPosition _ rover getPosition.

	(observerPosition ~= observedPosition) ifTrue: [
		observedPosition  _ observerPosition.
		self logPosition.	
	]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverPositionLogger class' category: 'MarsRover-WithHeading'!
RoverPositionLogger class
	instanceVariableNames: ''!

!RoverPositionLogger class methodsFor: 'instance creation' stamp: 'ljm 10/24/2022 20:40:31'!
for: aMarsRover  

	^ self new initializeFor: aMarsRover ! !


!classDefinition: #RoverWindow category: 'MarsRover-WithHeading'!
Object subclass: #RoverWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverWindow methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:07:16'!
initializeFor: aMarsRover 

	self subclassResponsibility! !


!RoverWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:07:31'!
update

	self subclassResponsibility! !


!RoverWindow methodsFor: 'accessing' stamp: 'ljm 10/25/2022 18:07:29'!
show

	self subclassResponsibility! !


!classDefinition: #RoverHeadingAndPositionWindow category: 'MarsRover-WithHeading'!
RoverWindow subclass: #RoverHeadingAndPositionWindow
	instanceVariableNames: 'rover content position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverHeadingAndPositionWindow methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	rover _ aMarsRover.
	self updatePosition.
	self updateHeading.
	
	rover setObserver: self.! !


!RoverHeadingAndPositionWindow methodsFor: 'accessing' stamp: 'ljm 10/25/2022 18:18:30'!
show

	^ position, ' - ', heading! !


!RoverHeadingAndPositionWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:04'!
update

	self updatePosition.
	self updateHeading.
	
	! !

!RoverHeadingAndPositionWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:04'!
updateHeading

	^ heading _ rover getHeading! !

!RoverHeadingAndPositionWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:20:57'!
updatePosition

	^ position _ rover getPosition printString! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverHeadingAndPositionWindow class' category: 'MarsRover-WithHeading'!
RoverHeadingAndPositionWindow class
	instanceVariableNames: ''!

!RoverHeadingAndPositionWindow class methodsFor: 'instance creation' stamp: 'ljm 10/25/2022 18:08:55'!
for: aMarsRover 

	^ self new initializeFor: aMarsRover ! !


!classDefinition: #RoverHeadingWindow category: 'MarsRover-WithHeading'!
RoverWindow subclass: #RoverHeadingWindow
	instanceVariableNames: 'rover heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverHeadingWindow methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	rover _ aMarsRover.
	self updateHeading.

	rover setObserver: self.! !


!RoverHeadingWindow methodsFor: 'accessing' stamp: 'ljm 10/25/2022 18:21:17'!
show

	^ heading! !


!RoverHeadingWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:43'!
update

	self updateHeading.! !

!RoverHeadingWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:43'!
updateHeading

	^ heading _ rover getHeading! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverHeadingWindow class' category: 'MarsRover-WithHeading'!
RoverHeadingWindow class
	instanceVariableNames: ''!

!RoverHeadingWindow class methodsFor: 'instance creation' stamp: 'ljm 10/25/2022 17:57:05'!
for: aMarsRover 

	^ self new initializeFor: aMarsRover ! !


!classDefinition: #RoverPositionWindow category: 'MarsRover-WithHeading'!
RoverWindow subclass: #RoverPositionWindow
	instanceVariableNames: 'rover position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!RoverPositionWindow methodsFor: 'initialization' stamp: 'ljm 10/25/2022 18:23:33'!
initializeFor: aMarsRover 

	rover _ aMarsRover.
	self updatePosition.	

	rover setObserver: self.! !


!RoverPositionWindow methodsFor: 'accessing' stamp: 'ljm 10/25/2022 18:21:22'!
show

	^ position ! !


!RoverPositionWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:33'!
update

	self updatePosition! !

!RoverPositionWindow methodsFor: 'update' stamp: 'ljm 10/25/2022 18:21:33'!
updatePosition

	^ position _ rover getPosition printString! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverPositionWindow class' category: 'MarsRover-WithHeading'!
RoverPositionWindow class
	instanceVariableNames: ''!

!RoverPositionWindow class methodsFor: 'instance creation' stamp: 'ljm 10/25/2022 17:50:38'!
for: aMarsRover 

	^ self new initializeFor: aMarsRover ! !
