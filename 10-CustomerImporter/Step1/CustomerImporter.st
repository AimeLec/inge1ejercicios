!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/10/2022 19:24:28'!
test01CustomersShouldBeImportedCorrectly
	
	| customer address |

	(CustomerImporter fromStream: self validDataInput withSession: session) import.
	
	self assertCustomersQuantity: 2.
	
	customer _ self customerFromDatabaseWithIDOfType: 'D' andNumber: '22333444'.				
	self assertCustomer: customer hasFirstName: 'Pepe' andLastName: 'Sanchez'.	
	self assertCustomer: customer hasAddressesQuantity: 2.
	address _ self addressOf: customer at: 'San Martin'.
	self
		assertAddress: address
		hasNumber: 3322
		inTown: 'Olivos'
		withZipCode: 1636
		inProvince: 'BsAs'.	

	address _ self addressOf: customer at: 'Maipu'.
	self
		assertAddress: address
		hasNumber: 888
		inTown: 'Florida'
		withZipCode: 1122
		inProvince: 'Buenos Aires'.	

	customer _ self customerFromDatabaseWithIDOfType: 'C' andNumber: '23-25666777-9'.				
	self assertCustomer: customer hasFirstName: 'Juan' andLastName: 'Perez'.	
	self assertCustomer: customer hasAddressesQuantity: 1.
	address _ self addressOf: customer at: 'Alem'.
	self
		assertAddress: address
		hasNumber: 1122
		inTown: 'CABA'
		withZipCode: 1001
		inProvince: 'CABA'.	
! !


!ImportTest methodsFor: 'setup - teardown' stamp: 'ljm 11/10/2022 19:32:44'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).! !

!ImportTest methodsFor: 'setup - teardown' stamp: 'ljm 11/10/2022 19:33:19'!
tearDown

	session close! !


!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:07:28'!
assertAddress: address hasNumber: addressNumber inTown: town withZipCode: zipCode inProvince: province    

	self assert:(
		address
			isInNumber: addressNumber
			andInTown: town
			andZipCode: zipCode
			andProvince: province
	)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:25:08'!
assertCustomer: aCustomer hasAddressesQuantity: aNumber 

	self assert: (aCustomer hasAddressesQuantity: aNumber)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:11:56'!
assertCustomer: customer hasFirstName: firstName andLastName: lastName  

	self assert: (customer hasFirstName: firstName andLastName: lastName)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 18:47:42'!
assertCustomersQuantity: aQuantity

	^ self assert: aQuantity equals: (session selectAllOfType: Customer) size! !


!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 18:55:36'!
addressOf: customer at: streetName 

	^ customer addressAt: streetName ifNone: [self fail]! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 19:15:00'!
customerFromDatabaseWithIDOfType: idType andNumber: idNumber 

	^ (session select: [ :aCustomer | aCustomer hasIDWithNumber: idNumber andType: idType] ofType: Customer) anyOne! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 18:30:58'!
validDataInput

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!Address methodsFor: 'comparison' stamp: 'ljm 11/10/2022 19:10:57'!
isInNumber: anStreetNumber andInTown: aTown andZipCode: aZipCode andProvince: aProvince 

	^ anStreetNumber = streetNumber and: [ aTown = town ] and: [ aZipCode = zipCode ] and: [ aProvince = province ]! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'ljm 11/10/2022 18:45:21'!
addressAt: aStreetName ifNone: aNoneBlock  

	^ addresses
		detect: [ :anAddress | anAddress streetName = aStreetName ]
		ifNone: [	"TODO: Hacer test"]
! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:25:24'!
hasAddressesQuantity: aNumber 

	^ aNumber = addresses size! !

!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:12:36'!
hasFirstName: aFirstName andLastName: aLastName 

	^ aFirstName = firstName and: [ aLastName = lastName ]! !

!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:15:47'!
hasIDWithNumber: anIDNumber andType: anIDType 

	^ anIDNumber = identificationNumber and: [ anIDType = identificationType ]! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'ljm 11/10/2022 19:02:44'!
initializeFromStream: aReadStream withSession: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'ljm 11/10/2022 19:33:31'!
import

	| newCustomer line |

	line := readStream nextLine.

	session beginTransaction.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth .
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth ].

		line := readStream nextLine. ].

	session commit.
	readStream close! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'ljm 11/10/2022 19:02:44'!
fromStream: aReadStream withSession: aSession
	^self new initializeFromStream: aReadStream withSession: aSession! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
