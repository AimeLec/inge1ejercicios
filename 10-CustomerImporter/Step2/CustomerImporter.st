!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 17:47:51'!
test01CustomersShouldBeImportedCorrectly

	(CustomerImporter fromStream: self validDataInput withSession: session) import.
	
	self assertCustomersQuantity: 2.
	self assertPepeSanchezIsImportedCorrectly.
	self assertJuanPerezIsImportedCorrectly.	! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:01:02'!
test02CannotImportARecordWithAnUnknownType

	self
		should: [ 	(CustomerImporter fromStream: self invalidRecordType withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordTypeErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:06:52'!
test03CannotImportARecordWithInvalidCustomerType

	self
		should: [ (CustomerImporter fromStream: self inputWithInvalidCustomerType withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordTypeErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:07:18'!
test04CannotImportWhenStreamHasAnEmptyLine

	self
		should: [ (CustomerImporter fromStream: self inputWithAnEmptyLine withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidStreamErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:07:32'!
test05CannotImportWithoutAnStream

	self
		should: [ (CustomerImporter fromStream: nil withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidStreamErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:02:34'!
test06CannotImportWithoutADatabaseSession

	self
		should: [ (CustomerImporter fromStream: self validDataInput withSession: nil) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidDatabaseSessionErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:07:59'!
test07CannotImportARecordWithInvalidAddressType

	self
		should: [ (CustomerImporter fromStream: self inputWithAnInvalidAddressType withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordTypeErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 1.
			self assertAddressesQuantity: 0.
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:08:34'!
test08CustomerRecordShouldHaveAtLeast5Attributes

	self
		should: [ (CustomerImporter fromStream: self inputWithNotEnoughCustomerAttributes withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordFormatErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:10:30'!
test09CustomerRecordShouldHaveAtMost5Attributes

	self
		should: [ (CustomerImporter fromStream: self inputWithTooManyCustomerAttributes withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordFormatErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:09:30'!
test10AddressRecordShouldHaveAtLeast6Attributes

	self
		should: [ (CustomerImporter fromStream: self inputWithNotEnoughAddressAttributes withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordFormatErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 1.
			self assertAddressesQuantity: 0.
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:09:49'!
test11AddressRecordShouldHaveAtMost6Attributes

	self
		should: [ (CustomerImporter fromStream: self inputWithTooManyAddressAttributes withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter invalidRecordFormatErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 1.
			self assertAddressesQuantity: 0.
		]
! !

!ImportTest methodsFor: 'tests' stamp: 'ljm 11/14/2022 19:10:12'!
test12CannotImportAnAddressWithoutACustomer

	self
		should: [ (CustomerImporter fromStream: self inputWithNoCustomers withSession: session) import ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: CustomerImporter customerNotPresentErrorMessage equals: anError messageText.
			self assertCustomersQuantity: 0.
			self assertAddressesQuantity: 0.
		]
! !


!ImportTest methodsFor: 'setup - teardown' stamp: 'ljm 11/10/2022 19:32:44'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).! !

!ImportTest methodsFor: 'setup - teardown' stamp: 'ljm 11/10/2022 19:33:19'!
tearDown

	session close! !


!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:07:28'!
assertAddress: address hasNumber: addressNumber inTown: town withZipCode: zipCode inProvince: province    

	self assert:(
		address
			isInNumber: addressNumber
			andInTown: town
			andZipCode: zipCode
			andProvince: province
	)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/14/2022 18:38:20'!
assertAddressesQuantity: aQuantity 

	^ self assert: aQuantity equals: (session selectAllOfType: Address) size! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:25:08'!
assertCustomer: aCustomer hasAddressesQuantity: aNumber 

	self assert: (aCustomer hasAddressesQuantity: aNumber)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 19:11:56'!
assertCustomer: customer hasFirstName: firstName andLastName: lastName  

	self assert: (customer hasFirstName: firstName andLastName: lastName)! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/10/2022 18:47:42'!
assertCustomersQuantity: aQuantity

	^ self assert: aQuantity equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/14/2022 17:47:45'!
assertJuanPerezIsImportedCorrectly

	| customer address |



	customer _ self customerFromDatabaseWithIDOfType: 'C' andNumber: '23-25666777-9'.				
	self assertCustomer: customer hasFirstName: 'Juan' andLastName: 'Perez'.	
	self assertCustomer: customer hasAddressesQuantity: 1.
	address _ self addressOf: customer at: 'Alem'.
	self
		assertAddress: address
		hasNumber: 1122
		inTown: 'CABA'
		withZipCode: 1001
		inProvince: 'CABA'! !

!ImportTest methodsFor: 'assertions' stamp: 'ljm 11/14/2022 17:46:56'!
assertPepeSanchezIsImportedCorrectly
	
	| customer address |

	customer _ self customerFromDatabaseWithIDOfType: 'D' andNumber: '22333444'.				
	self assertCustomer: customer hasFirstName: 'Pepe' andLastName: 'Sanchez'.	
	self assertCustomer: customer hasAddressesQuantity: 2.
	address _ self addressOf: customer at: 'San Martin'.
	self
		assertAddress: address
		hasNumber: 3322
		inTown: 'Olivos'
		withZipCode: 1636
		inProvince: 'BsAs'.	

	address _ self addressOf: customer at: 'Maipu'.
	self
		assertAddress: address
		hasNumber: 888
		inTown: 'Florida'
		withZipCode: 1122
		inProvince: 'Buenos Aires'.	! !


!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 18:55:36'!
addressOf: customer at: streetName 

	^ customer addressAt: streetName ifNone: [self fail]! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 19:15:00'!
customerFromDatabaseWithIDOfType: idType andNumber: idNumber 

	^ (session select: [ :aCustomer | aCustomer hasIDWithNumber: idNumber andType: idType] ofType: Customer) anyOne! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:07:07'!
inputWithAnEmptyLine

	^ ReadStream on: '
'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:07:52'!
inputWithAnInvalidAddressType

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
AA,San Martin,3322,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:06:22'!
inputWithInvalidCustomerType

	^ ReadStream on: 'CC,Pepe,Sanchez,D,22333444'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:10:05'!
inputWithNoCustomers

	^ ReadStream on: 'A,San Martin,3322,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:09:23'!
inputWithNotEnoughAddressAttributes

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:08:30'!
inputWithNotEnoughCustomerAttributes

	^ ReadStream on: 'C,Pepe,Sanchez,D'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:09:43'!
inputWithTooManyAddressAttributes

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs,EXTRA'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 19:09:01'!
inputWithTooManyCustomerAttributes

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444,EXTRA'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/14/2022 18:09:51'!
invalidRecordType

	^ ReadStream on: 'X'! !

!ImportTest methodsFor: 'support' stamp: 'ljm 11/10/2022 18:30:58'!
validDataInput

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!Address methodsFor: 'comparison' stamp: 'ljm 11/10/2022 19:10:57'!
isInNumber: anStreetNumber andInTown: aTown andZipCode: aZipCode andProvince: aProvince 

	^ anStreetNumber = streetNumber and: [ aTown = town ] and: [ aZipCode = zipCode ] and: [ aProvince = province ]! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'ljm 11/10/2022 18:45:21'!
addressAt: aStreetName ifNone: aNoneBlock  

	^ addresses
		detect: [ :anAddress | anAddress streetName = aStreetName ]
		ifNone: [	"TODO: Hacer test"]
! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:25:24'!
hasAddressesQuantity: aNumber 

	^ aNumber = addresses size! !

!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:12:36'!
hasFirstName: aFirstName andLastName: aLastName 

	^ aFirstName = firstName and: [ aLastName = lastName ]! !

!Customer methodsFor: 'testing' stamp: 'ljm 11/10/2022 19:15:47'!
hasIDWithNumber: anIDNumber andType: anIDType 

	^ anIDNumber = identificationNumber and: [ anIDType = identificationType ]! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream line record newCustomer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'ljm 11/14/2022 18:26:12'!
initializeFromStream: aReadStream withSession: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'ljm 11/14/2022 18:37:32'!
isAddressRecord

	^ record first = 'A'! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'ljm 11/14/2022 18:13:41'!
isCustomerRecord

	^ record first = 'C'! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'ljm 11/14/2022 18:08:15'!
thereAreRecordsToImport

	line := readStream nextLine.
	^ line notNil! !


!CustomerImporter methodsFor: 'validations' stamp: 'ljm 11/14/2022 18:53:51'!
validateCustomerPresent

	newCustomer ifNil: [ ^ self error: self class customerNotPresentErrorMessage ].! !

!CustomerImporter methodsFor: 'validations' stamp: 'ljm 11/14/2022 18:27:51'!
validateRecord

	record isEmpty ifTrue: [ ^ self error: self class invalidStreamErrorMessage ]! !

!CustomerImporter methodsFor: 'validations' stamp: 'ljm 11/14/2022 18:53:07'!
validateRecordHasQuantityOfFields: aQuantity

	record size = aQuantity ifFalse: [ ^ self error: self class invalidRecordFormatErrorMessage ].
! !


!CustomerImporter methodsFor: 'import' stamp: 'ljm 11/14/2022 18:08:05'!
createRecord

	^ record := line findTokens: $,! !

!CustomerImporter methodsFor: 'import' stamp: 'ljm 11/14/2022 18:08:58'!
import

	session beginTransaction.

	[self thereAreRecordsToImport ] whileTrue: [  
		self createRecord.
		self importRecord.
	 ].

	session commit.
	readStream close! !

!CustomerImporter methodsFor: 'import' stamp: 'ljm 11/14/2022 18:54:12'!
importAddress

	| newAddress |

	self validateRecordHasQuantityOfFields: 6.
	self validateCustomerPresent.

	newAddress := Address new.
	newCustomer addAddress: newAddress.
	newAddress streetName: record second.
	newAddress streetNumber: record third asNumber .
	newAddress town: record fourth.
	newAddress zipCode: record fifth asNumber .
	newAddress province: record sixth! !

!CustomerImporter methodsFor: 'import' stamp: 'ljm 11/14/2022 18:53:24'!
importCustomer
	
	self validateRecordHasQuantityOfFields: 5.

	newCustomer := Customer new.
			newCustomer firstName: record second.
			newCustomer lastName: record third.
			newCustomer identificationType: record fourth.
			newCustomer identificationNumber: record fifth .
			session persist: newCustomer! !

!CustomerImporter methodsFor: 'import' stamp: 'ljm 11/14/2022 18:20:47'!
importRecord

	self validateRecord.

	self isCustomerRecord ifTrue: [ ^ self importCustomer ].
	self isAddressRecord ifTrue: [ ^ self importAddress ].
	
	^ self error: self class invalidRecordTypeErrorMessage! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'ljm 11/14/2022 18:32:50'!
fromStream: aReadStream withSession: aSession
	self validateReadStream: aReadStream andDatabaseSession: aSession.

	^ self new initializeFromStream: aReadStream withSession: aSession! !


!CustomerImporter class methodsFor: 'errors' stamp: 'ljm 11/14/2022 18:51:41'!
customerNotPresentErrorMessage

	^ 'Customer must be present'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'ljm 11/14/2022 18:33:36'!
invalidDatabaseSessionErrorMessage

	^ 'Invalid database session'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'ljm 11/14/2022 18:40:07'!
invalidRecordFormatErrorMessage

	^ 'Invalid record format'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'ljm 11/14/2022 18:11:03'!
invalidRecordTypeErrorMessage

	^ 'Invalid record type'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'ljm 11/14/2022 18:28:06'!
invalidStreamErrorMessage

	^ 'Invalid stream'! !


!CustomerImporter class methodsFor: 'validations' stamp: 'ljm 11/14/2022 18:30:53'!
validateIsNotNil: aReadStream 
	self shouldBeImplemented.! !

!CustomerImporter class methodsFor: 'validations' stamp: 'ljm 11/14/2022 18:33:15'!
validateReadStream: aReadStream andDatabaseSession: databaseSession 

	aReadStream ifNil: [ ^ self error: self invalidStreamErrorMessage ].
	databaseSession ifNil: [ ^ self error: self invalidDatabaseSessionErrorMessage ]! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
