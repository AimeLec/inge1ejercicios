!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:41:48'!
* aMultiplier

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:41:45'!
+ anAdder

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:41:51'!
- aSubtrahend

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:41:54'!
/ aDivisor

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:24:24'!
addToEntero: anEntero

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:26:37'!
addToFraccion: aFraccion

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:32:01'!
divideByEntero: anEntero

	^ self subclassResponsibility .! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:32:07'!
divideByFraccion: aFraccion

	^ self subclassResponsibility .! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:28:39'!
multiplyByEntero: anEntero

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:28:44'!
multiplyByFraccion: anEntero

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:00:47'!
negate
	^ self! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:45:17'!
subtractFromEntero: anEntero

	^ self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:45:23'!
subtractFromFraccion: aFraccion

	^ self subclassResponsibility ! !


!Numero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:41:08'!
isNegative

	^ self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:41:12'!
isOne

	^ self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:41:15'!
isZero

	^ self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de n�mero inv�lido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:31:22'!
* aMultiplier 
	
	^ aMultiplier multiplyByEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:27:51'!
+ anAdder 
	
	^ anAdder addToEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:51:35'!
- aSubtrahend 
	
	^ aSubtrahend subtractFromEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:34:05'!
/ aDivisor 
	
	^ aDivisor divideByEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:55'!
// aDivisor 
	
	^self class with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:53:50'!
addToEntero: anAdderOfTypeEntero
	
	^ anAdderOfTypeEntero class with: (anAdderOfTypeEntero integerValue +	 self integerValue).! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:54:01'!
addToFraccion: anAdderOfTypeFraccion
	
	^ (Entero with: (anAdderOfTypeFraccion denominator integerValue * self integerValue + anAdderOfTypeFraccion numerator integerValue )) / (Entero with: anAdderOfTypeFraccion denominator integerValue )! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:55:07'!
divideByEntero: aDividendOfTypeEntero

	^ Fraccion with: aDividendOfTypeEntero over: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:55:19'!
divideByFraccion: aDividendOfTypeFraccion

	^  aDividendOfTypeFraccion numerator / (aDividendOfTypeFraccion denominator * self).! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:16:36'!
fibonacci

	^ self subclassResponsibility .! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:56:01'!
multiplyByEntero: aMultiplierOfTypeEntero
	
	^ self class with: value * aMultiplierOfTypeEntero integerValue.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:56:19'!
multiplyByFraccion: aMultiplierOfTypeFraccion
	
	^  self * aMultiplierOfTypeFraccion numerator / aMultiplierOfTypeFraccion denominator.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:56:46'!
subtractFromEntero: minuendOfTypeEntero

	^ minuendOfTypeEntero class with: (minuendOfTypeEntero integerValue - self integerValue ).! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:56:55'!
subtractFromFraccion: minuendOfTypeFraccion

	^ (Entero with: (minuendOfTypeFraccion numerator integerValue - (minuendOfTypeFraccion denominator integerValue * self integerValue))) / (Entero with: minuendOfTypeFraccion denominator integerValue ).
! !


!Entero methodsFor: 'comparing' stamp: 'ljm 9/12/2022 21:42:26'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:35:19'!
isNegative
	
	^ self subclassResponsibility.! !

!Entero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:37:07'!
isOne

	^ self subclassResponsibility.! !

!Entero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:35:16'!
isZero

	^ self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'ljm 9/14/2022 19:11:02'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	| subclaseDeEntero |

	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].	

	aValue < 0 ifTrue: [ subclaseDeEntero _ Negativo].
	aValue = 0 ifTrue: [ subclaseDeEntero _ Cero].
	aValue = 1 ifTrue: [ subclaseDeEntero _ Uno].
	aValue > 1 ifTrue: [ subclaseDeEntero _ MayorAUno].	
	
	^ subclaseDeEntero new initalizeWith: aValue.! !


!classDefinition: #Cero category: 'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:16:17'!
fibonacci

	^ Entero with: 1.! !

!Cero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:25:24'!
initializeFraccionFor: aDivisor

	^ self.
	! !

!Cero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:52:35'!
initializeWithEnteroGreaterThanOneDividend: aDividend

	^ self error: (Numero canNotDivideByZeroErrorDescription)! !

!Cero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:52:39'!
initializeWithNegativeDividend: aDividend

	^ self error: (Numero canNotDivideByZeroErrorDescription)! !

!Cero methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:52:42'!
initializeWithUnoAsDividend

	^ self error: (Numero canNotDivideByZeroErrorDescription)! !


!Cero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:35:53'!
isNegative
	^ false! !

!Cero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:35:37'!
isOne
	^ false! !

!Cero methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:35:42'!
isZero
	^ true! !



!classDefinition: #MayorAUno category: 'Numero-Exercise'!
Entero subclass: #MayorAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!MayorAUno methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:18:37'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci.
		! !

!MayorAUno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:26:02'!
initializeFraccionFor: aDivisor

	^ aDivisor initializeWithEnteroGreaterThanOneDividend: self.
	! !

!MayorAUno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:29:54'!
initializeWithEnteroGreaterThanOneDividend: aDividend

	| denominator greatestCommonDivisor numerator |

	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	denominator isOne ifTrue: [ ^numerator ].
	
	^ Fraccion new initializeWith: numerator over: denominator.! !

!MayorAUno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:39:13'!
initializeWithNegativeDividend: aDividend

	| denominator greatestCommonDivisor numerator |

	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	denominator isOne ifTrue: [ ^numerator ].
	
	^ Fraccion new initializeWith: numerator over: denominator.
	! !

!MayorAUno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:23:45'!
initializeWithUnoAsDividend

	^ Fraccion new initializeWith: (Entero with: 1) over: self! !


!MayorAUno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:23'!
isNegative
	^ false! !

!MayorAUno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:18'!
isOne
	^ false! !

!MayorAUno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:20'!
isZero
	^ false! !


!classDefinition: #Negativo category: 'Numero-Exercise'!
Entero subclass: #Negativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Negativo methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:15:29'!
fibonacci
	
	^ self error: Entero negativeFibonacciErrorDescription.! !

!Negativo methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:36:23'!
initializeFraccionFor: aDivisor

	^ aDivisor initializeWithNegativeDividend: self.
	! !

!Negativo methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:33:20'!
initializeWithEnteroGreaterThanOneDividend: aDividend

	^ aDividend negated / self negated! !

!Negativo methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:37:42'!
initializeWithNegativeDividend: aDividend

	^ aDividend negated / self negated
	! !

!Negativo methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:24:26'!
initializeWithUnoAsDividend

	^ Fraccion new initializeWith: (Entero with: -1) over: self negated! !


!Negativo methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:42'!
isNegative
	^ true! !

!Negativo methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:33'!
isOne
	^ false! !

!Negativo methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:37'!
isZero
	^ false! !


!classDefinition: #Uno category: 'Numero-Exercise'!
Entero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 21:23:28'!
fibonacci
	
	^ self.
! !

!Uno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:21:37'!
initializeFraccionFor: aDivisor

	^ aDivisor initializeWithUnoAsDividend.
	! !

!Uno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:33:55'!
initializeWithEnteroGreaterThanOneDividend: aDividend

	^ aDividend! !

!Uno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:39:38'!
initializeWithNegativeDividend: aDividend

	^ aDividend! !

!Uno methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:22:12'!
initializeWithUnoAsDividend

	^ self! !


!Uno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:59'!
isNegative
	^ false! !

!Uno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:53'!
isOne
	^ true! !

!Uno methodsFor: 'testing' stamp: 'ljm 9/12/2022 21:36:57'!
isZero
	^ false! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:31:34'!
* aMultiplier 
	
	^ aMultiplier multiplyByFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:28:00'!
+ anAdder 
	
	^ anAdder addToFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:51:24'!
- aSubtrahend 
	
	^ aSubtrahend subtractFromFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/12/2022 20:33:53'!
/ aDivisor 
	
	^ aDivisor divideByFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:57:50'!
addToEntero: anAdderOfTypeEntero

	| newNumerator newDenominator |
	
	newNumerator := denominator * anAdderOfTypeEntero + numerator.
	newDenominator := denominator.
	
	^ newNumerator / newDenominator 		! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:57:58'!
addToFraccion: anAdderOfTypeFraccion

	| newNumerator newDenominator |
	
		newNumerator := numerator * anAdderOfTypeFraccion denominator + (denominator * anAdderOfTypeFraccion numerator).
		newDenominator := denominator * anAdderOfTypeFraccion denominator.
		
		^ newNumerator / newDenominator 		! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:05'!
divideByEntero: aDividendOfTypeEntero

	^  denominator * aDividendOfTypeEntero / numerator! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:14'!
divideByFraccion: aDividendOfTypeFraccion

	^  denominator * aDividendOfTypeFraccion numerator / (numerator * aDividendOfTypeFraccion denominator)! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:35'!
multiplyByEntero: aMultiplierOfTypeEntero

	^ numerator * aMultiplierOfTypeEntero  / denominator.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:41'!
multiplyByFraccion: aMultiplierOfTypeFraccion

	^ numerator * aMultiplierOfTypeFraccion numerator / (denominator * aMultiplierOfTypeFraccion denominator).! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:49'!
subtractFromEntero: minuendOfTypeEntero

	| newNumerator newDenominator |
		
	newNumerator :=  denominator * minuendOfTypeEntero - numerator .
	newDenominator := denominator.
	
	^ newNumerator / newDenominator 		! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ljm 9/14/2022 17:58:55'!
subtractFromFraccion: minuendOfTypeFraccion

	| newNumerator newDenominator |
	
	" c/d - numerator/denominator = (c*denominator - d* numerator)/d*denom "	
		
	newNumerator :=  denominator * minuendOfTypeFraccion numerator - (numerator * minuendOfTypeFraccion denominator).
	newDenominator := denominator * minuendOfTypeFraccion denominator.
	
	^newNumerator / newDenominator ! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'ljm 9/14/2022 18:00:18'!
with: aDividend over: aDivisor

	^ aDividend initializeFraccionFor: aDivisor.! !
